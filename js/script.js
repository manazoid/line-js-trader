const ctx = document.getElementById('myChart').getContext('2d');
const myChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: [],
        datasets: [{
            label: ['Нажми сюда'],
            data: [],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 2
        }]
    },
    options: {
        scales: {
            y: {
                beginAtZero: true
            }
        }
    }
});


console.log("Чарт готов")

myChart.render()
let second = 5;

function CheckMin(array) {
    // for (let check in array) {
        let min = Math.min(array)
        return min
    // }
}

setInterval(() => {
    let rnd = Math.floor(Math.random() * 20);

    myChart.data.labels.push(second);
    
    // console.log(`End push ${myChart.data.labels.length}`)
    
    myChart.data.datasets[0].data.push(rnd)
    second++;

    console.log(CheckMin(myChart.data.datasets[0].data));

    myChart.update();
}, 1000)



